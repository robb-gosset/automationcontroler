

struct channel {
  int  direction; // 0 none 1 down 2 up
  bool run; // 0 stop 1 run
  int  directionPin;
  int  runPin;
  int  upPin;
  int  downPin;
} channells[3];

int  runAll = 6;
int  keySwitch = 5;
int  eStopSense = A3;
int  eStopEnable = A4;
int  eStopReset = 7;
bool stopped = 0;

void setup() {

  Serial.begin(9600);

  channells[0].directionPin = A0;
  channells[0].upPin = 8;
  channells[0].downPin = 9;
  channells[0].runPin = 2;
  channells[1].directionPin = A1;
  channells[1].upPin = 10;
  channells[1].downPin = 11;
  channells[1].runPin = 3;
  channells[2].directionPin = A2;
  channells[2].upPin = 12;
  channells[2].downPin = 13;
  channells[2].runPin = 4;

  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);        // 3 down
  pinMode(12, OUTPUT);        // 3 up
  pinMode(11, OUTPUT);        // 2 down
  pinMode(10, OUTPUT);        // 2 up
  pinMode(9,  OUTPUT);        // 1 down
  pinMode(8,  OUTPUT);        // 1 up
  pinMode(7,  INPUT_PULLUP);  // Reset
  pinMode(6,  INPUT_PULLUP);  // Run All
  pinMode(5,  INPUT_PULLUP);  // Keyswitch
  pinMode(4,  INPUT_PULLUP);  // 3 run
  pinMode(3,  INPUT_PULLUP);  // 2 run
  pinMode(2,  INPUT_PULLUP);  // 1 run
  pinMode(A0, INPUT);         // 1 direction
  pinMode(A1, INPUT);         // 2 direction
  pinMode(A2, INPUT);         // 3 direction
  pinMode(A3, INPUT_PULLUP);  // E-Stop Sense
  pinMode(A4, OUTPUT);        // E-Stop Enable

  digitalWrite(A4, HIGH);

  for ( int pin = 2; pin <= 13; pin ++)
  {
    digitalWrite(pin, HIGH);
  }


}

// the loop function runs over and over again forever
void loop() {
  Serial.print("stopped:");
  Serial.println(stopped);
  // Read E-Stop and enable pins
  if (digitalRead(eStopSense))
  {
    digitalWrite(eStopEnable, HIGH);
    stopped = 1;
  }

  if (stopped)
  {
    for ( int channel = 0; channel < 3; channel ++)
    {
      digitalWrite(channells[channel].upPin, HIGH);
      digitalWrite(channells[channel].downPin, HIGH);
    }
    if (!digitalRead(keySwitch) && !digitalRead(eStopReset))
    {
      digitalWrite(eStopEnable, LOW);
      stopped = 0;
    }
  }

  if ((!digitalRead(keySwitch)) && !stopped)
  {
    // Read direction pins
    for ( int channel = 0; channel < 3; channel ++)
    {
      int value = analogRead(channells[channel].directionPin);

      if (value > 600)
      {
        channells[channel].direction = 1;
        continue;
      } else if (value < 450)
      {
        channells[channel].direction = 2;
        continue;
      } else
      {
        channells[channel].direction = 0;
      }
    }

    // Set direction outputs
    for ( int channel = 0; channel < 3; channel ++)
    {
      if ((!digitalRead(channells[channel].runPin)) || (digitalRead(runAll)))
      {
        switch (channells[channel].direction)
        {
          case 0:
            digitalWrite(channells[channel].upPin, HIGH);
            digitalWrite(channells[channel].downPin, HIGH);
            break;

          case 1:
            digitalWrite(channells[channel].upPin, HIGH);
            digitalWrite(channells[channel].downPin, LOW);
            break;

          case 2:
            digitalWrite(channells[channel].upPin, LOW);
            digitalWrite(channells[channel].downPin, HIGH);
            break;

          default:
            digitalWrite(channells[channel].upPin, HIGH);
            digitalWrite(channells[channel].downPin, HIGH);
            break;
        }
      }
      else
      {
        digitalWrite(channells[channel].upPin, HIGH);
        digitalWrite(channells[channel].downPin, HIGH);
      }
    }
  }
  else
  {
    for ( int channel = 0; channel < 3; channel ++)
    {
      digitalWrite(channells[channel].upPin, HIGH);
      digitalWrite(channells[channel].downPin, HIGH);
    }
  }
  delay(2);
}
